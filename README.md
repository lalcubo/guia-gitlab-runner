# guia gitlab-runner
Con esta Guía aprenderá  a correr gitlab-runner en un servidor para comunicarse a otro servidor que tengas instalado gitlab

Asumiendo  que tienes instalado:

- gitlab en un servidor A con su usuario creado en el mismo
- servidor B donde se instalara el gitlab-runner con sus respectivos pasos



1) Abrimos una consola en el Servidor B donde se generara la llave pública  y privada ssh 
2) Una vez que tenga la consola abierta debe correr el siguiente comando:
```
ssh-keygen -t rsa -C "nombre@gmail.com" -b 4096
```

  Una vez que corran el comando anterior te pedira donde desea grabar tu archivo de llave pública  y privada por defecto queda en:

```
Generating public/private rsa key pair.
Enter file in which to save the key (/home/tu-usuario/.ssh/id_rsa):
```

  Puedes  cambiar la ruta pero la dejare por defecto presionamos enter
  Lo siguiente nos pedirá  una frase la dejare en blanco:
```  
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
```

  Al presionar enter nos generara la llave creando 2 archivos la llave pública  `id_rsa.pub`y la privada `id_rsa`
  Para ver tu llave publica correr el siguiente comando:

>  cat id_rsa.pub

Copias el texto que te da ese archivo para ser grabado en el servidor gitlab (servidor A)



3) Nos vamos al perfil luego en setting para agregar la llave ssh y evitar estar colocando usuario y clave cada vez que se haga push o pull

    - En el menú hacer clic en SSH Keys
    - En el cuadro de texto copiar la llave publica generada anteriormente 
    - En title puede salir o no el nombre basado en la llave pública puedes dejar el que viene por defecto o colocar un nombre para que lo identifiques bien
    - Presionar Add key y listo ya tiene guardado si llave publica


4) Este paso es opcional solo debes usarlo si la conexión ssh al servidor es distinto al puerto por defecto (22) 
    - En mi caso el puerto no es 22 si no 65099
    - Crear un archivo llamado config dentro de la carpeta .ssh con sus permisos 600 del archivo 
    
- NOTA En caso de existir el archivo editelo y no hacer el echo si no agregar el puerto
```
echo "Port 65099" > config
chmod 600 config
```

5) En el servidor donde tienes instalado gitlab web crear un proyecto en mi caso lo llame `prueba-cicd` lo coloca público y guarda los cambios

6) Entramos al proyecto `prueba-cicd` y nos saldrá la URL del proyecto para poder clonarlo en mi caso `gitlab@gitlab.xxxxxxx.com:lsanchez/prueba-cicd.git`

7) para probar que la llave publica se guardo bien en gitlab clonamos el proyecto creado anteriormente (estará vacío ya que no se le creo ningún archivo)
    - La primera vez nos pedirá que aceptemos agregar el servidor a los host conocido escribimos yes
    - Creo una carpeta llamada prueba donde clonare

```
mkdir prueba
cd prueba
git clone gitlab@gitlab.xxxxxxx.com:lsanchez/prueba-cicd.git
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '[gitlab.xxxxxxx.com]:65099,[10.0.10.102]:65099' (ECDSA) to the list of known hosts.
remote: Counting objects: 47, done.
remote: Compressing objects: 100% (39/39), done.
remote: Total 47 (delta 9), reused 29 (delta 3)
Receiving objects: 100% (47/47), 4.57 KiB | 0 bytes/s, done.
Resolving deltas: 100% (9/9), done.
```


8) Instalamos gitlab-runner en el Servidor donde se quiere que haga el despliegue automáticamente en mi caso seguiré usando el servidor B
 - El servidor de prueba es 64bit (CentOS) por lo que se bajo es la librería para el según la requerida.
 - https://docs.gitlab.com/runner/install/linux-manually.html

```
curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/rpm/gitlab-runner_amd64.rpm
sudo rpm -i gitlab-runner_amd64.rpm
```

- Instalado el `gitlab-runner` (servidor B) podemos registrar un runner para que funcione con el gitlab (servidor A) de forma automática
- Vamos al servidor web de gitlab entramos al proyecto `prueba-cicd` al final panel izquierdo dice setting entramos en esa opción y luego:
  - Entramos a CI/CD
  - En Runners le damos clic en donde dice expand.
  - En la opción donde dice 'Setup a specific Runner manually' anotamos la dirección que nos da (URL) más el token:
```
Specify the following URL during the Runner setup: 
    https://gitlab.xxxxxxx.com/
Use the following registration token during setup: 
    K8b8naZpAvpwEeSBZxxx
```
9) Registrar un runner:
   - Primero pide el url
   - Luego pide el token
   - Nombre que le quieres asignar al runner
   - Alguna etiqueta  para búsquedas en mi caso lo deje en blanco
   - Como correr el archivo yml que se creara mas tarde en mi caso shell porque son comandos de consola

```
sudo gitlab-runner register
Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
https://gitlab.xxxxxxx.com/
Please enter the gitlab-ci token for this runner:
K8b8naZpAvpwEeSBZxxx
Please enter the gitlab-ci description for this runner:
[desarrollo3]: prueba
Please enter the gitlab-ci tags for this runner (comma separated):

Registering runner... succeeded                     runner=K8b8naZp
Please enter the executor: docker+machine, docker-ssh+machine, kubernetes, docker, parallels, ssh, virtualbox, custom, dock           er-ssh, shell:
shell
```

10) Si volvemos al gitlab web y refrescamos notamos que en donde dice `Runners activated for this project` sale un ID y el nombre que le dimos en este caso `prueba`

11) Creación el archivo .gitlab-ci.yml con la siguiente estructura:

```
stages:
  - init
  - despliegue

hello:
  stage: init
  script:
    - echo "hola mundo!"
    - whoami

deploy:
  stage: despliegue
  script:
    - cd /home/lsanchez/deploy/prueba-cicd
    - git pull gitlab@gitlab.xxxxxxx.com:lsanchez/prueba-cicd.git

```
- NOTA donde está el stage init es solo para colocar otro ejemplo de consola


12) Guarda los cambios con:
 - git add .gitlab-ci.yml
 - git commit -m "archivo yml para deploy"
 - git push

- Nota: Si vas a tu proyecto en el gitlab web luego en donde dice CI/CD -> pipelines te va a generar un error ya que en el archivo yml creado anteriormente no existe esa carpeta llamada deploy ni esta clonado el proyecto en deploy
 

13) Creo otra carpeta llamada deploy 
```
mkdir deploy
cd deploy
git clone gitlab@gitlab.xxxxxxx.com:lsanchez/prueba-cicd.git
```

14) Para el usuario gitlab-runner que se crear al instalar gitlab-runner debes crear la una llave pública y privada y agregarla al proyecto:
  - Ver paso 2 
  - Ver paso 3 (opcional si no usas el puerto por defecto)
  - También hacer un git clone del proyecto y aceptar los host conocidos para que no vuelva a pedir eso y funcione con normalidad los pipelines cuando corra el YML
  - Luego borrar la carpeta para que no quede ese proyecto en el usuario gitlab-runner 

15) Puedes crear en la carpeta prueba un archivo cualquiera por ejemplo index.html y lo subes a gitlab

```
echo "hola mundo" > index.html
git add *
git commit -m "primera linea del index"
git push
```

16) vuelves a tu proyecto en gitlab web en la opción CI/CD -> pipelines y veras que ahora ya no da el error
    - Si vas a tu carpeta deploy veras que ya sale solo el index.html y todos los cambios que haga en la carpeta prueba